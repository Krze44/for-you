import keyboard
import time
import win32gui
import mouse
time.sleep(5)
i = 0
# hwnd = win32gui.FindWindow(None, "Apex Legends")
# win32gui.SetForegroundWindow(hwnd)
# time.sleep(2)


    
def activate_window(title):
    hwnd = win32gui.FindWindow(None, title)
    if hwnd != 0:
        win32gui.SetForegroundWindow(hwnd)
    else:
        print(f"Error: Window '{title}' not found")

def execute_commands(file_path):
    hotkeys = {}
    with open(file_path, 'r') as f:
        commands = f.read().splitlines()
        
    for command in commands:
        # if command[0] == "/":
        #     continue
        if "<Hotkey " in command:
            hotkey = command.split()[1].strip(">")
            def find_next_hotkey():
                next_hotkey_index = len(commands)
                for i, cmd in enumerate(commands):
                    if "<Hotkey " in cmd:
                        next_hotkey_index = i
                        break
                
        if "<key " in command:
            key = command.split()[1].strip(">")
            keyboard.press(key)
            confirmation = f'Pressed key: {key}'
        elif "<wait " in command:
            wait_time = float(command.split()[1].strip(">"))
            time.sleep(wait_time)
            confirmation = f'waited for {wait_time} seconds'
        elif "<release " in command:
            key = command.split()[1].strip(">")
            keyboard.release(key)
            confirmation = f'released key: {key}'
        elif "<text " in command:
            text = command.split(maxsplit=1)[1].strip(">")
            time.sleep(0.5)
            keyboard.write(text, delay=0.01)
            confirmation = f'wrote text: {text}'
        else:
            confirmation = f'invalid command: {command}'
        print(confirmation)

# activate_window("Apex Legends")
# execute_commands("text.txt")


# while i < 3:
#     time.sleep(0.01)
#     keyboard.press("w")
#     time.sleep(2)
#     keyboard.press("ctrl")
#     time.sleep(0.35)
#     keyboard.release("ctrl")
#     keyboard.press("spacebar")
#     time.sleep(0.35)
#     keyboard.release("spacebar")
#     time.sleep(0.001)
#     keyboard.release('w')
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     mouse.wheel(10)
#     time.sleep(0.001)
#     keyboard.press("w")
#     time.sleep(2)
#     keyboard.release('w')
#     i += 1

activate_window("EverQuest")
execute_commands("text.txt")
while True:
    continue